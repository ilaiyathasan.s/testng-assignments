package com.testng;

import static org.testng.Assert.assertEquals;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class actiTimeText_LogoCheck {
		WebDriver driver;
	
	 @BeforeClass
	  public void launchBrowser() {
		    driver = new ChromeDriver();
			driver.get("https://demo.actitime.com/login.do");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	  }
	
  @Test
  public void checkText() {
	  WebElement headertext = driver.findElement(By.id("headerContainer"));
	  String actualtext = headertext.getText();
	  String expectedtext = "Please identify yourself";
	  actualtext.contains(expectedtext);
	  
  }
  
  @Test
  public void checkLogo() {
	  WebElement logo = driver.findElement(By.xpath("//div[@class='atLogoImg']"));
	  boolean actuallogo = logo.isDisplayed();
	  assertEquals(actuallogo,true);
  }
  
  @AfterClass
  public void closeBrowser() {
  	driver.quit();
  }
}
