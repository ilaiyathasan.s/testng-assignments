package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Test2dependsOnMethods {
	WebDriver driver;
	@Test(dependsOnMethods = "newCustomer")
	  public void ModifyCustomer() {
		  System.out.println("The Customer gets modified");
	  }
	  @Test(dependsOnMethods = "ModifyCustomer")
	  public void createCustomer() {
		  System.out.println("The Customer gets created");
	  }
	  @Test
	  public void newCustomer() {
		  System.out.println("The new Customer gets created");
	  }
	  @BeforeMethod
	  public void beforeCustomer() {
		  System.out.println("Verifying the method");
	  }
	  @AfterMethod
	  public void afterCustomer() {
		  System.out.println("All the transactions are done");
	  }
	 
	  @BeforeClass
	  public void beforeClassMethod() {
		  System.out.println("Start database connection, Launch browser");
	  }
	  @AfterClass
	  public void afterClassMethod() {
		  System.out.println("Close database connection, Close browser");
	  }
}
