package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class GroupingTest {
	
  @BeforeClass
	public void beforeclass() {
		 System.out.println("Start database connection, Launch browser");
	}
  
  @Test(groups={"Smoke Test"})
  public void createCustomer() {
	  System.out.println("customer gets created");
  }
  @Test(groups={"Regression Test"})
  public void newCustomer() {
	  System.out.println("New customer gets created");
  }
  @Test(groups= {"Usability Test"})
  public void modifyCustomer() {
	  System.out.println("customer gets modified");
  }
  @Test(groups={"Smoke Test"})
  public void changeCustomer() {
	  System.out.println("customer gets changed");
  }
  
  @AfterClass
  public void afterClassMethod() {
	  System.out.println("Close database connection, Close browser");
  }
}
