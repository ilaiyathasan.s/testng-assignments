package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class demoWebShop {
	WebDriver driver;
	 @BeforeClass
	  public void beforeClassMethod() {
		  driver = new ChromeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://demowebshop.tricentis.com/");
	  }
	 @Test
	 public void login() {
		 driver.findElement(By.linkText("Log in")).click();
		 driver.findElement(By.id("Email")).sendKeys("misterthasan24@gmail.com");
		 driver.findElement(By.id("Password")).sendKeys("123456");
		 driver.findElement(By.xpath("//input[@value='Log in']")).click();
	 }
	 
	 @Test(dependsOnMethods = "login")
	 public void addToCart() throws InterruptedException {
		 WebElement computer=driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));
			Actions act = new Actions(driver);
			act.moveToElement(computer).build().perform();
			WebElement desktop=driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Cell phones')]"));
			act.moveToElement(desktop).click().perform();
			driver.findElement(By.xpath("//a[text()='Smartphone']")).click();
			driver.findElement(By.id("add-to-cart-button-43")).click();
		
	 }
	
	  @BeforeMethod
	  public void beforeCustomer() {
		  System.out.println(driver.getTitle());
	  }
	  @AfterMethod
	  public void afterCustomer() {
		  String actualtitle=driver.getTitle();
			String expecttitle="Demo Web Shop";
			if (actualtitle.equalsIgnoreCase(expecttitle)) {
				System.out.println("Pass");
				
			}
			else {
				System.out.println("Fail");
			}
			//assert(actualtitle.equals(expecttitle));
	  }
	 
	  @AfterClass
	  public void afterClassMethod() {
		  driver.quit();
	  }
}

