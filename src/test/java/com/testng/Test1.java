package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Test1 {
  @Test(priority = 1)
  public void ModifyCustomer() {
	  System.out.println("The Customer gets modified");
  }
  @Test(priority = 3)
  public void createCustomer() {
	  System.out.println("The Customer gets created");
  }
  @Test(priority = 2)
  public void newCustomer() {
	  System.out.println("The new Customer gets created");
  }
  @BeforeMethod
  public void beforeCustomer() {
	  System.out.println("Verifying the method");
  }
  @AfterMethod
  public void afterCustomer() {
	  System.out.println("All the transactions are done");
  }
  @BeforeClass
  public void beforeClassMethod() {
	  System.out.println("Start database connection, Launch browser");
  }
  @AfterClass
  public void afterClassMethod() {
	  System.out.println("Close database connection, Close browser");
  }
}
